

if($.trim(finacialYear) != '')
nextYear			=	Number(finacialYear)+Number(1);	
startDate			=	finacialYear+"-04-01";
endDate				=	nextYear+"-03-31";
//console.log(startDate+"  "+endDate);
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function pagePrint(){
		
		$(".ignoreInPrint").css("display",'none');
		$("#printarea").css("width",'100%');
		$(".heading").css("display",'');
		window.print();
		$("#printarea").css("width",'75%');
		$(".ignoreInPrint").css("display",'');
		$(".heading").css("display",'none');
}
	

$(document).on("click",".submitFormData",function(){
	
	var getBillNo		=	0;
	var error			=	0;
	
	if(!Validate()){
			$('html, body').animate({
							scrollTop: $(".startCode").offset().top
					}, 1000);
			$(".checkError").css('display','');	
			$(".emessage").html('The filetype you are attempting to upload is not allowed.');
			setTimeout(hideSuccessMessage('checkError'), 5000);	
			error	=	1;
	}
	
	if($(".iAgrreText-view").is(":checked")){
			$(".billNumber").each(function() {
				if($.trim($(this).val()) == ''){
					$(this).css('border','solid 1px red');
					error 	=	1;
				}
				getBillNo++;
			});
			$(".totalValue").each(function() {
				if($.trim($(this).val()) == ''){
					$(this).css('border','solid 1px red');
					error 	=	1;
				}
			});	
			if(getBillNo == 0 || error!= 0){
				
				if(getBillNo == 0){
					$('html, body').animate({
									scrollTop: $(".startCode").offset().top
							}, 1000);
					$(".checkError").css('display','');	
					$(".emessage").html('Please Fill atleast one Bill');
					setTimeout(hideSuccessMessage('checkError'), 5000);	
				}
			}else{
				$('.xyz').submit();
			}
	}else{
			alert("Please Click on CheckBox");
	}
});

$(document).on("blur",".billNumber",function(){
	$(this).css('border','1px solid #e5e5e5');
});	
$(document).on("blur",".totalValue",function(){
	
	var id				=	$(this).attr("color");
	var totalData		=	0;
	$(this).css('border','1px solid #e5e5e5');
	$(".custom"+id+" .totalValue").each(function () {
		totalData		=	 Number($(this).val()) + Number(totalData);
	});	
	
	$(".totalBillSubmited"+id).val(totalData);
	$(".totalValueOfBill"+id).val(totalData);
	
	
});
	
$(document).on("click",".remove",function(){
	var id				=	$(this).attr("color");
	var counter			=	0;
	$(".subData"+id+" table tbody tr").each(function () {
		counter++;
	
	});
	
	if(counter != '0'){
		$(".custom"+id+" tbody tr:last").remove();	
		if(counter == '1')
		$(".subData"+id+"").css("display","none");	
	}
	
});
	
$(document).on("click",".add",function(){
	
	var id			=	$(this).attr("color");
	var data		=	$(this).attr("data-attr");
	var counter		=	0;
	//var toDayDate	=	getTodayDate();
	var flag		=	1;
	
	$(".subData"+id+" table tbody tr").each(function () {
		counter++;
		flag++;
	});
	
	if(counter == 0){
		flag			=	1;
		$(".subData"+id).css("display","");
	}
	
	if(limit >= flag){
		$(".subData"+id+" table tbody").append('<tr class="first_'+flag+'"><td style="text-align:center;">'+flag+'</td><td style="text-align:left;"><div class="input-group date" id="datetimepicker"><input type="text" readonly value="" class="form-control forEffectiveForm'+flag+' effective_form" name="effective_form['+data+'][]" placeholder="dd/mm/yyyy" value="" ><span color="'+flag+'" class="input-group-addon sub"><span class="glyphicon glyphicon-calendar"></span></span></div></td><td><input maxlength="15" type="text" class="form-control billNumber" name="billNumber['+data+'][]" maxlength="15" placeholder="Bill No"></td><td><input maxlength="80" type="text" name="particular['+data+'][]" value="" class="form-control ASA_yearly"></td><td><input type="text" name="totalValue['+data+'][]" value="" maxlength="7" color="'+id+'" onkeypress="return isNumber(event)" class="form-control totalValue"></td><td><input type="file" name="attachment['+data+'][]" class="form-control attachment_file" id="attachment"></td></tr>');
		flag++;
		$('.effective_form').datepicker({
				changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
				dateFormat: "dd/mm/yy",
				minDate :new Date(startDate),
				maxDate: new Date() 
		});
	}
});	

                                  
$(document).on("click",".parent",function(){
		 $( ".effective_form").focus();
});

$(document).on("click",".sub",function(){
		var id = $(this).attr('color');
		$( ".forEffectiveForm"+id).focus();
});	
jQuery(document).ready(function(){
	
	$('.effective_form').datepicker({
				changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
				dateFormat: "dd/mm/yy",
				minDate:new Date()
	});
	
	
	$(".subOption").live("click",function(){
		var id = $(this).attr('color');
		$( ".subData"+id ).slideToggle( "slow", function() {
											if($(".changeimage").attr("color") == "1"){
												$(".changeimage").removeClass( "glyphicon-plus" ).addClass( "glyphicon-minus" );
												$(".changeimage").attr("color","2");
											}else{
												$(".changeimage").removeClass( "glyphicon-minus" ).addClass( "glyphicon-plus" )
												$(".changeimage").attr("color","1");
											}
											 
		});	
	});
});

function getTodayDate(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!

	var yyyy = today.getFullYear();
	if(dd<10){
		dd='0'+dd;
	} 
	if(mm<10){
		mm='0'+mm;
	} 
	return today = dd+'/'+mm+'/'+yyyy;
}

function hideSuccessMessage($className) {     
    $("."+$className+"").fadeOut(6000);
	
}

var validFileExtensions = ["jpg","jpeg","bmp","gif","png","pdf","doc","xml","docx","txt","ppt","xlsx","xls","pptx"];  
  
function Validate() {
	
	var getTotalError	=	0;
	$('.attachment_file').each(function(){
		
		var fileName 		=	$.trim($(this).val());
		
		if(fileName != ''){
				var file_error	=	0;
				var res = fileName.split('.');
				var lengthOfExtension 		=		res.length;
				var count 					=		Number(lengthOfExtension)-Number(1);
				//.toUpperCase();
				for (var j = 0; j < validFileExtensions.length; j++) {
					if($.trim(res[count].toUpperCase()) == $.trim(validFileExtensions[j].toUpperCase())){
						file_error++;
						break;
					}		
				}	
			if(file_error == 0){
				$(this).css('border','1px solid red');
				getTotalError++;
			}	
		}
		
	});
	
	if(getTotalError == 0){
		return true;
	}else{
		return false;
    }
  
}