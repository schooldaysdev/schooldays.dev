$(document).on("keydown",".maxLength", function(e){ 

        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
});
$(document).on("click",".reimbursementConfig_submit", function(e){ 
	
	var formData 				= 	$("#myModal #myForm1").serializeArray();
	var typeOfReimbursement		=	$("#myModal .typeOfReimbursement").val();
	var error 					=	0;
	if($.trim($("#myModal .typeOfReimbursement").val()) == '0'){
		error					=	1;
		$("#myModal .error_reimbursement").css("display","");
	}
	if($.trim($("#myModal .maxLength").val()) == ''){
		error					=	1;
		$("#myModal .error_max_length").css("display","");
	}
	
	if(error == '0'){
		$("#myModal .help-block").css("display","none");
		$("#myModal .ajax_loader").css("display","");
		$.ajax({
				type:"POST",
				url: siteUrl+"/reimbursement_configration/save_configration",
				data: {type:'save_configration',data:formData,reimbursement:typeOfReimbursement},
				complete:function (data) {
					$("#myModal1 .ajax_loader").css("display",'none');
				},	
				success: function (data) {
						var data1 	=	JSON.parse(data);
						if(data1.error	==	'Error'){
							$("#myModal .checkError1").css("display","");
							$("#myModal .checkError1 .emessage1").text(data1.errorMessage);	
							setTimeout(hideSuccessMessage('checkError1'),7000);
						}else{
							$("#myModal .checkSuccess1").css("display","");
							$("#myModal .checkSuccess1 .smessage1").text("Configration Setting Inserted Successfully !");	
							setTimeout(hideSuccessMessage('checkSuccess1'),7000);
							refreshPage();
								//window.location.href=site_url+'flexipay_configration/';
						}	
				}
		});
	}

});
$(document).on("click",".reimbursementConfig_edit", function(e){ 
	
	var formData 				= 	$("#myForm2").serializeArray();
	var typeOfReimbursement		=	$("#myModal1 .typeOfReimbursement").val();
	var id						=	$(this).attr('color');
	
	var error 					=	0;
	if($.trim($("#myModal1 .typeOfReimbursement").val()) == '0'){
		error					=	1;
		$("#myModal1 .error_reimbursement").css("display","");
	}
	if($.trim($("#myModal1 .maxLength").val()) == ''){
		error					=	1;
		$("#myModal1 .error_max_length").css("display","");
	}
	
	if(error == '0'){
		$("#myModal1 .help-block").css("display","none");
		$("#myModal1 .ajax_loader").css("display","");
		$.ajax({
				type:"POST",
				url: siteUrl+"/reimbursement_configration/save_configration",
				data: {type:'save_configration',data:formData,reimbursement:typeOfReimbursement,id:id},
				complete:function (data) {
					$(".ajax_loader").css("display",'none');
				},	
				success: function (data) {
						var data1 	=	JSON.parse(data);
						if(data1.error	==	'Error'){
							$("#myModal1 .checkError1").css("display","");
							$("#myModal1 .checkError1 .emessage1").text(data1.errorMessage);	
							setTimeout(hideSuccessMessage('checkError1'),7000);
						}else{
							$("#myModal1 .checkSuccess1").css("display","");
							$("#myModal1 .checkSuccess1 .smessage1").text("Configration Setting Inserted Successfully !");	
							setTimeout(hideSuccessMessage('checkSuccess1'),7000);
							refreshPage();
								//window.location.href=site_url+'flexipay_configration/';
						}	
				}
		});
	}

});

function refreshPage(){
	
	$.ajax({
				type:"POST",
				url: siteUrl+"/reimbursement_configration/refreshPage",
				data: {type:'refreshPage'},
				complete:function (data) {
					
				},	
				success: function (data) {
						var data1 	=	JSON.parse(data);
						$("#data").html(data1.data);
				}
		});
	
}

function hideSuccessMessage($className) {     
    $("."+$className+"").fadeOut(6000);
 }

$(document).on("click",".status",function(){
		
		var data 	=	$.trim($(this).html());
		var id 		=	$(this).attr('color');
		var type	=	$(this).attr('data-attr');
		if(data	==	'Active'){
			data 	=	'0';
			$(this).html('In Active');
		}else{
			data	=	'1';
			$(this).html('Active');
		}
		
		$.ajax({
				type:"POST",
				url: siteUrl+"/flexipay_configration/fbp_component_active",
				data: {type:type,id:id,data:data},
				complete:function (data) {
				},	
				success: function (data) {
						
				}
		}); 
	}); 
