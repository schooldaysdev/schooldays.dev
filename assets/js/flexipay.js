
jQuery(document).ready(function() {
	
		$('#valid_from').datepicker({
				changeMonth: true,
                changeYear: true,
                format: "dd/mm/yyyy",
				dateFormat: "dd/mm/yy",
				minDate:new Date()
		});
		
        $(".close").click(function() {
            $(".alert-success").css("display", "none");
        });
        // alert("Hello");
        $(".HMO,.form_position,.field_category").keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
		
		$("input[name='optionsRadios2']").change(function(){
			var value	=	$('input[name=optionsRadios2]:checked').val();
			//onsole.log(value);
			if(value	==	'Select Box'){
				$('#myModal').modal('show');
			}
		});
		
		$(".HMOSUBMIT").click(function(){
			var valueOfTextBox		=	$('.HMO').val();
			$(".save_info").css("display","");
			if(valueOfTextBox != '0'){
				if (!$(".save_info")[0]){
					$(".modal-footer").html('<button type="button" color="'+k+'" class="btn green save_info pull-right">Submit</button><img class="loadingImage" src="'+siteUrl+'/assets/img/ajax-loader.gif" style="display:none;margin-left:5px;" /> ');
				}
				$(".fieldDesc").css('display','');
				for(var i=1;i<=valueOfTextBox;i++){
					$('table .selectField').append('<tr class="deleteRow'+counter+'"><td>'+counter+'<input type="hidden" value="" maxlength="50" class="form-control id" name="id[]" placeholder=""></td><td><input type="text" maxlength="50" class="form-control fieldValue" name="field_value[]" placeholder="Field Value"></td><td><input maxlength="100" type="text" class="form-control" name="field_desc[]" placeholder="Field Description"></td><td> <button type="button" color="'+counter+'" class="btn btn-default btn-sm deleteData"> <span class="glyphicon glyphicon-trash"></span> Trash </button></td></tr>');
					counter++;
				}
			}
		});
		
		
		
    });
	
	
	$(document).on("change",".statusChange",function(){
		//alert("Hello");
		var id 		=	$(this).attr('color');
		var data	=	$(this).val();
		//alert(id);
		if($.trim(data) != '' && $.trim(id)!= ''){
			$.ajax({
				type:"POST",
				url: siteUrl+"/flexipay_configration/fbp_component_active",
				data: {type:'flexipay_sub_cat',id:id,data:data},
				complete:function (data) {
				},	
				success: function (data) {
						
						//counter	=	Number(counter)-Number(1);
				}
			});	
		}
		
	});
	
	$(document).on("click",".deleteData",function(){
		
		var id 		=	$(this).attr('color');
		
		if ($(".inserted"+id+"")[0]){
			$(".deleteRow"+id+"").remove();
			$.ajax({
				type:"POST",
				url: siteUrl+"/flexipay_configration/fbp_subcomponent_delete",
				data: {type:'delete',id:id},
				complete:function (data) {
				},	
				success: function (data) {
						
						//counter	=	Number(counter)-Number(1);
				}
			});	
		}else{	
			$(".deleteRow"+id+"").remove();
			counter	=	Number(counter)-Number(1);
		}
		
	});
	
	$(document).on("change",".payheadInFbp",function(){
			var id = $(this).val();
			console.log(id);
			if(id == '0')
				$(".inPayheadSectionAllowed").css('display','none');	
			else
				$(".inPayheadSectionAllowed").css('display','');
			
			
	});
	
	$(document).on("change",".field_category1",function(){
			
			var id = $(this).val();
			
			if(id == '1'){
				$(".payheadType").css('display','');
				$('.payheadInFbpSection').css('display','none');
			
			}else if(id == '2'){
				$('.payheadInFbpSection').css('display','');
				$(".payheadType").css('display','none');	
			}else{
				$(".payheadType").css('display','none');	
				
			}
	
	});
	
	$(document).on("blur",".valid_from",function(){
		$('.validFrom').css('display','none');
	});
	
	$(document).on("click",".save_info",function(){
		
		var error 	=	'0';
		var jsonObj 			 = 	[];
		var data 	=	''
		var flag = 0;
		var dataBind = '';
		var countVA	=	0;
		var idOFBP	=	$.trim($(this).attr("color"));
		var error	= 0;
		$(".selectField input").each(function() {
		
				if(flag == '2'){
					
					if($.trim(data1) != ''){
						item = {}
						data 			=	$(this).val()+" ~~ "+data+" ~~ "+data1;
						item[countVA]   = 	data;
						data			=	'';
						jsonObj.push(item);
						countVA++;
						error		=	1;
					}else{
						data			=	'';
					}
				}else if(flag == '0'){
					data			=	$(this).val();
				}else if(flag == '1'){
					data1			=	$(this).val();
				}
			flag++;
			if(flag == '3'){
				flag = 0;	
			}
		});
		//console.log(JSON.stringify(jsonObj));
		if(error == '1'){	
			$(".loadingImage").css("display",'');
			$.ajax({
				type:"POST",
				url: siteUrl+"/flexipay_configration/sub_option",
				data: {type:'suboption',data:jsonObj,color:idOFBP},
				complete:function (data) {
					$(".loadingImage").css("display",'none');
				},	
				success: function (data) {
						var data1 	=	JSON.parse(data);
						if(data1.error	==	'Error'){
							$(".checkError1").css("display","");
							$(".checkError1 .emessage1").text(data1.errorMessage);	
							setTimeout(hideSuccessMessage('checkError1'),7000);
						}else{
								
								$(".save_info").css("display","none");
								$(".save_info").attr("color",data1.color);
								$(".submitForm").attr("color",data1.color);
								$(".smessage1").text('Record Successfully Inserted !');
								$(".checkSuccess1").css('display','');
								setTimeout(hideSuccessMessage('checkSuccess1'), 5000);
						}	
				}
			});
		}else{
			$(".checkError1").css("display","");
			$(".checkError1 .emessage1").text("Please Fill Atleast One Field Value");	
			setTimeout(hideSuccessMessage('checkError1'),7000);
		}	
			
	});
		
function hideSuccessMessage($className) {     
	
    $("."+$className+"").fadeOut(6000);
   
 }	
 
function xyz(){
	
	var FBP				=	$.trim($(".submitForm").attr("color"));
	
	var formData 		= 	$("#myForm").serializeArray();
	var valueOfRadio	=	$('input[name=optionsRadios2]:checked').val();
	var error			=	0;
	var editType		=	$.trim($(".submitForm").attr("data-attr"));
	//console.log(formData);
	//console.log($('#myForm').serialize());
	$( ".payhead_name" ).keypress(function() {
		$(".namePayHEAD").css("display","none");
	})
	$( ".valid_from" ).keypress(function() {
		$(".validFrom").css("display","none");
	})
	$( ".form_position" ).keypress(function() {
		$(".positionInForm").css("display","none");
	})
	$( ".payhead_desc" ).keypress(function() {
		$(".descPayHEAD").css("display","none");
	})
	$( ".field_category" ).change(function() {
		$(".FBPField").css("display","none");
	})	
	if($.trim($(".payhead_name").val()) == ''){
		
		$(".namePayHEAD").css("display","");
		error	=	1;
	}
	
	if($.trim($(".valid_from").val()) == ''){
		$(".validFrom").css("display","");
		error	=	1;
	}
	
	
	if($.trim($(".field_category1").val()) == '1'){
		if($.trim($(".form_position").val()) == ''){
			$(".positionInForm").css("display","");
			error	=	1;
		}
	}else if($.trim($(".field_category1").val()) == '2' && $.trim($('.payheadInFbp').val()) == '1'){
		if($.trim($(".form_position").val()) == ''){
			$(".positionInForm").css("display","");
			error	=	1;
		}
	}
	
	if($.trim($(".field_category").val()) == '' && ($(".field_category1").val() == '1')){
		$(".FBPField").css("display","");
		error	=	1;
	}else if($.trim($(".field_category1").val()) == '2' && $.trim($('.payheadInFbp').val()) == '1'){
		if($.trim($(".field_category").val()) == ''){
			$(".FBPField").css("display","");
			error	=	1;
		}
	}
	
	if($.trim($(".payhead_desc").val()) == ''){
		$(".descPayHEAD").css("display","");
		error	=	1;
	}
	
	if(valueOfRadio		==	'Select Box'){
		if(FBP	==	''){
				$(".checkError").css("display","");
				$(".checkError .emessage").text("Please Insert Sub Options of Payhead");	
				setTimeout(hideSuccessMessage('checkError'),7000);
				error	=	1;
		}
	}else{
		if(editType != 'RWRpdC1kYXRh')
		FBP				=	'';
	}
	

	if(error		==	'0'){
		$(".loading_image").css("display","");
		$.ajax({
				type:"POST",
				url: siteUrl+"/flexipay_configration/fbp_component",
				data: {type:'save_fbpComponent',data:formData,color:FBP,editType:editType},
				complete:function (data) {
					$(".loading_image").css("display",'none');
				},	
				success: function (data) {
						var data1 	=	JSON.parse(data);
						if(data1.error	==	'Error'){
							$(".checkError").css("display","");
							$(".checkError .emessage").text(data1.errorMessage);	
							setTimeout(hideSuccessMessage('checkError'),7000);
						}else{
								window.location.href=siteUrl+'flexipay_configration/';
						}	
				}
			});
	}else{
		return false;
	}	
}

