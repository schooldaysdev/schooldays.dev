
	<div class="page-content-wrapper">
		<div class="page-content">
				
									<div class="portlet light bordered form-fit">
										<div class="portlet-title">
											<div class="caption">
												<i class="icon-user font-blue-hoki"></i>
												<span class="caption-subject font-blue-hoki bold uppercase"><?php if(isset($editInfo['ARN'])){ echo "Company Information Edit"; }else{ echo "Student Registration"; } ?></span>
												<span class="caption-helper">(Enter student's basic information)</span>
											</div>
											
										</div>
										<div class="portlet-body form">
											<!-- BEGIN FORM-->
											<form action="" name="companyInfo" class="form-horizontal form-bordered form-row-stripped companyInfo" method="POST">
												
												<div class="form-body">
													<?php
													if($this->session->userdata('Is_Admin') && $this->session->userdata('Is_Admin') == '1'){
													?>
														<div class="form-group">
															<label class="control-label col-md-3">Username<span class="requiredValidate"></span></label>
															<div class="col-md-9">
																<input maxlength="30" type="text" placeholder="UserName." value="<?php if(isset($editInfo['userName'])){ echo $editInfo['userName']; }?>" name="userName" class="form-control required">
																<span class="help-block">
																Please enter UserName ! </span>
															</div>
														</div>
														<div class="form-group">
															<label class="control-label col-md-3">Password <span class="requiredValidate"></span></label>
															<div class="col-md-9">
																<input maxlength="30" type="text" placeholder="Password" value="<?php if(isset($editInfo['Password'])){ echo $editInfo['Password']; }?>" name="Password" class="form-control required">
																<span class="help-block">
																Please enter Password ! </span>
															</div>
														</div>
													<?php
													}
													?>	
													<div class="form-group">
														<label class="control-label col-md-3">ARN No. <span class="requiredValidate">*</span></label>
														<div class="col-md-9">
															<input maxlength="30" type="text" placeholder="ARN No." value="<?php if(isset($editInfo['ARN'])){ echo $editInfo['ARN']; }?>" name="ARN" class="form-control required">
															<span class="help-block">
															Please enter ARN no ! </span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">GST No <span class="requiredValidate">*</span></label>
														<div class="col-md-9">
															<input maxlength="30" type="text" value="<?php if(isset($editInfo['GST'])){ echo $editInfo['GST']; }?>" placeholder="GST NO." name="GST" class="form-control required">
															<span class="help-block">
															Please enter GST no !</span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Pan No <span class="requiredValidate">*</span></label>
														<div class="col-md-9">
															<input maxlength="11" type="text" value="<?php if(isset($editInfo['PAN'])){ echo $editInfo['PAN']; }?>" placeholder="PAN NO" name="PAN" class="form-control required">
															<span class="help-block">
															Please enter Pan no ! </span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Name Of Company <span class="requiredValidate">*</span></label>
														<div class="col-md-9">
															<input maxlength="50" type="text" value="<?php if(isset($editInfo['name'])){ echo $editInfo['name']; }?>" name="comapnyName" placeholder="Name of Company" class="form-control required">
															<span class="help-block">
															Please enter company name !</span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Type of buisness <span class="requiredValidate">*</span></label>
														<div class="col-md-9">
															<input maxlength="50" type="text" value="<?php if(isset($editInfo['BUSINESS'])){ echo $editInfo['BUSINESS']; }?>" name="businessName" placeholder="Type of Buisness" class="form-control required">
															<span class="help-block">
															Please enter type of buisness ! </span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Address 1 <span class="requiredValidate">*</span></label>
														<div class="col-md-9">
															<textarea name="address1" class="form-control required"><?php if(isset($editInfo['ADDRESS1'])){ echo $editInfo['ADDRESS1']; }?></textarea>
															<span class="help-block">
															Please enter Address !</span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Address 2</label>
														<div class="col-md-9">
															<textarea name="address2" class="form-control "><?php if(isset($editInfo['ADDRESS2'])){ echo $editInfo['ADDRESS2']; }?></textarea>
															
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">State <span class="requiredValidate">*</span></label>
														<div class="col-md-9">
															<select name="state" class="form-control required state">
																<option value="0">Please select State</option>
																<?php
																if(isset($getState) && !empty($getState)){
																	foreach($getState as $keys=>$values){
																?>
																		<option value="<?php echo $keys; ?>"><?php echo $values; ?></option>
																<?php
																	}
																}
																?>
															</select>
															<span class="help-block">please enter state </span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">City <span class="requiredValidate">*</span></label>
														<div class="col-md-9">
															<select name="city" class="form-control city">
																<option value="0">Please select City</option>
															</select>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Email <span class="requiredValidate">*</span></label>
														<div class="col-md-9">
															<input name="email"  value="<?php if(isset($editInfo['EMAIL'])){ echo $editInfo['EMAIL']; }?>" maxlength="50" type="text" data-attr="email" class="form-control required">
															<span class="help-block">
															Please enter email id </span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Mobile <span class="requiredValidate">*</span></label>
														<div class="col-md-9">
															<input name="mobile" value="<?php if(isset($editInfo['MOBILE'])){ echo $editInfo['MOBILE']; }?>" maxlength="10" type="text" data-attr="mobile" class="form-control required numeric mobile">
															<span class="help-block">
															Please enter mobile number </span>
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-md-3">Pincode</label>
														<div class="col-md-9">
															<input name="pincode" value="<?php if(isset($editInfo['PINCODE'])){ echo $editInfo['PINCODE']; }?>" maxlength="5" type="text" data-attr="pincode" class="form-control required numeric pincode">
															<span class="help-block">
															Please enter pincode ! </span>
														</div>
													</div>
													<?php if(isset($companyId)){  ?>
														<input type="hidden" name="information" value="<?php echo $companyId; ?>">
													<?php
													}
													?>
												</div>
												<div class="form-actions">
													<div class="row">
														<div class="col-md-offset-3 col-md-9">
															<?php
															if(!isset($error)){
															?>
																<button type="button"  class="btn green submitButton"><i class="fa fa-check"></i> Submit</button>
																
															<?php
															}
															?>
															<button type="button" class="btn default cancel">Cancel</button>
														</div>
													</div>
												</div>
											</form>
											<!-- END FORM-->
										</div>
									</div>

					
		</div>

    </div>
</div>
<style>
.requiredValidate{
	color:red;
}
.help-block{
	color:red;
	font-size:11px;
	display:none;
}
</style>
<script>
var siteUrl	=	"<?php echo base_url(); ?>";
var k =1;
var counter	=	"<?php echo $iCount = 1; ?>";
    jQuery(document).ready(function() {
        
		Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
		
		$(".close").click(function(){
			$(".alert-success").css("display","none");
		});
		$('.eCode').select2();
		$('.payhead').select2();
		
    });
	
	
	$(document).on("change",".state",function(){
		var state 	=	$(this).val();
		$.ajax({
					type:"POST",
					url: siteUrl+"/registration/companyDeclaration/getCityList/"+state,
					async:false,
					complete:function (data) {
						
					},	
					success: function (data) {
						var data1 	=	JSON.parse(data);
						if(data1.error == ''){
							$(".city").html(data1.state);
						}
					}
		});
	});
	
	$(document).on("blur",".required",function(){
			$(this).next("span").css("display", "none");
	});
	
	$(document).on("click",".cancel",function(){
		window.location.href	=	siteUrl+"/registration/companyDeclaration/companyListing";
	});	
	
	$(document).on("click",".HMOSUBMIT",function(){
			//alert($('.HMO').val());
			var valueOfTextBox		=	$('.HMO').val();
			$(".save_info").css("display","");
			if(valueOfTextBox != '0'){
				if (!$(".save_info")[0]){
					$(".modal-footer").html('<button type="button" color="'+k+'" class="btn green save_info pull-right">Submit</button><img class="loadingImage" src="'+siteUrl+'/assets/img/ajax-loader.gif" style="display:none;margin-left:5px;" /> ');
				}
				$(".fieldDesc").css('display','');
				for(var i=1;i<=valueOfTextBox;i++){
					$('table .selectField').append('<tr class="deleteRow'+counter+'"><td>'+counter+'<input type="hidden" value="" maxlength="50" class="form-control id" name="id[]" placeholder=""></td><td><input type="text" maxlength="50" class="form-control fieldValue" name="field_value[]" placeholder="Field Value"></td><td><input maxlength="100" type="text" class="form-control" name="field_desc[]" placeholder="Field Description"></td><td> <button type="button" color="'+counter+'" class="btn btn-default btn-sm deleteData"> <span class="glyphicon glyphicon-trash"></span> Trash </button></td></tr>');
					counter++;
				}
			}
	});	
	$(document).on("click",".deleteData",function(){
		
		var id 		=	$(this).attr('color');
		
		if ($(".inserted"+id+"")[0]){
			$(".deleteRow"+id+"").remove();
			/*$.ajax({
				type:"POST",
				url: siteUrl+"/flexipay_configration/fbp_subcomponent_delete",
				data: {type:'delete',id:id},
				complete:function (data) {
				},	
				success: function (data) {
						
						//counter	=	Number(counter)-Number(1);
				}
			});	*/
		}else{	
			$(".deleteRow"+id+"").remove();
			counter	=	Number(counter)-Number(1);
		}
		
	});
	$(document).on("click",".save_info",function(){
		var error 	=	0;
		
		var formData = new FormData($("#myForm1")[0]);
		$(".loadingImage").css("display","");
		$.ajax({
							url:siteUrl+"registration/companyDeclaration/serviceCodeSave/",
							type:'POST',
							data:formData,
							contentType: false,
							cache: false,
							processData: false,
							success:function(result){
								var data1 	=	JSON.parse(result);
								$(".loadingImage").css("display","none");
								if(data1.error	==	'Error'){
									
									$(".checkError1").css("display","");
									$(".emessage1").text(data1.errorMessage);
									$('html, body').animate({
											scrollTop: $(".startCode").offset().top
									}, 1000);
									setTimeout(hideSuccessMessage('checkError1'),10000);
									
								}else{
									
									$(".checkSuccess1").css("display","");
									$(".smessage1").text(data1.errorMessage);
									$('html, body').animate({
											scrollTop: $(".startCode").offset().top
									}, 1000);
									setTimeout(hideSuccessMessage('checkSuccess1'),10000);
								}
								
								
							}
		});
	});	
	$(document).on("click",".submitButton",function(){
		var error = 0;
		var information	=	$(this).attr('data-attr');
		$(".required").each(function(){
			var attribute 		=	$(this).attr('data-attr');
			
			if($.trim($(this).val()) == '' || $(this).val() == '0'){
				error	=	1;
				$(this).next("span").css("display", "block"); 
			}else{
				if(attribute == 'email'){
						if( !isValidEmailAddress($.trim($(this).val())) ) {
							error	=	2;
							alert("Please Enter Valid Email id");
						}
				}else if(attribute == 'mobile'){
					var mobile = $(this).val();
					if((mobile.length)< 10 || (mobile.length)>10){
						error	=	3;
						alert("mobile should only be 10 digits");
					}
				}else if(attribute == 'pincode'){
					
					var pincode = $(this).val();
					if((pincode.length)< 5 || (pincode.length)>5){
						error	=	4;
						alert("pincode should only be 5 digits");
					}
					
				}
			}
		});
		//console.log(error);
		if(error == 0){
			$(".companyInfo").submit();
		}
	});
	
		$(".numeric").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
					 // let it happen, don't do anything
					 return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});
	
	function isValidEmailAddress(emailAddress) {
		var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		return pattern.test(emailAddress);
	}
	function hideSuccessMessage($className) {     
    $("."+$className+"").fadeOut(6000);
	}	
</script>
