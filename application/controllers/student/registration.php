<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends MY_Controller   {

	public function __construct()
	{
		parent::__construct();
		//$this->load->model('Flexipay_declaration_model');
		//$this->load->model('Student_model');
		$this->load->helper('download');
	}
	
	public function index()
	{
		$data['page_title'] = 'Schooldays | Registration';	
		$this->middle = 'student/student_registration'; // passing middle to function. change this for different views.
        $this->layout($data);
	}
	
	
	

}	