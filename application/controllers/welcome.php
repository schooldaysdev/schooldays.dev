<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller  {
    public function index() {
		$data 				=	array();
		$data['page_title'] = 'Schooldays - Dashboard!';
        $this->middle = 'dashboard'; // passing middle to function. change this for different views.
        $this->layout($data);
    }
}
