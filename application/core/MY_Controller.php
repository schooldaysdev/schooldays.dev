<?php
/**
 * Created by PhpStorm.
 * User: peeush.sharma
 * Date: 3/2/2017
 * Time: 5:44 PM
 */

 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller
{
	
    //Load layout
	function __Construct() {
		//$this->load->helper('url');
		parent::__construct();
		$this->load->library('session');
		parse_str($_SERVER['QUERY_STRING'], $_GET); 
	}
	
    public function layout($data = array()) {
        // making temlate and send data to view.
        $template = array();
        $template['header']   = $this->load->view('layout/header', $data, true); // To load header
        $template['sidemenu']   = $this->load->view('layout/sidemenu', $data, true); // To load side menu
        $template['middle'] = $this->load->view($this->middle, $data, true); // To load dynamic content 
        $template['footer'] = $this->load->view('layout/footer', $data, true); // To load footer
        $this->load->view('layout/index', $template); // render whole templete to view
    }
	
	function encrypt_decrypt($action, $string) {
		$output = false;

		$encrypt_method = "AES-256-CBC";
		$secret_key = 'This is my secret key';
		$secret_iv = 'This is my secret iv';

		// hash
		$key = hash('sha256', $secret_key);
		
		// iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
		$iv = substr(hash('sha256', $secret_iv), 0, 16);

		if( $action == 'encrypt' ) {
			$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
			$output = base64_encode($output);
		}
		else if( $action == 'decrypt' ){
			$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
		}

		return $output;
	}
}

?>